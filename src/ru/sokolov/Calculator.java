package ru.sokolov;

import java.io.IOException;

public class Calculator {

    private Calculator() {
    }

    public static double calculate(double firstNumber, double secondNumber, char operation) throws IOException {
        double result;
        switch (operation) {
            case '+':
                result = firstNumber + secondNumber;
                break;
            case '-':
                result = firstNumber - secondNumber;
                break;
            case '*':
                result = firstNumber * secondNumber;
                break;
            case '/':
                result = firstNumber / secondNumber;
                break;
            default:
                throw new IOException("Неправильный знак");
        }
        return result;
    }
}
