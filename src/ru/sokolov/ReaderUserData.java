package ru.sokolov;

public class ReaderUserData {

    private double firstNumber;
    private double secondNumber;
    private char operand;


    public void insertAndParseData(String text) {
        String[] textBlocks = text.split("[+-/*]");
        firstNumber = Double.parseDouble(textBlocks[0]);
        secondNumber = Double.parseDouble(textBlocks[1]);
        operand = text.charAt(textBlocks[0].length());
    }

    public double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public char getOperand() {
        return operand;
    }

    public void setOperand(char operand) {
        this.operand = operand;
    }
}
