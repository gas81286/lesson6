package ru.sokolov;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите выражение(если хотите прекратить работу калкулятора введите end):");
        boolean stop = false;
        while(!stop) {
            String text = scanner.nextLine();
            if(text.equals("end")){
                stop = true;
                continue;
            }

            try {
                ReaderUserData readerUserData = new ReaderUserData();
                readerUserData.insertAndParseData(text);

                double result = Calculator.calculate(readerUserData.getFirstNumber(), readerUserData.getSecondNumber(), readerUserData.getOperand());
                System.out.println("Результат = " + result);
            } catch (Exception e) {
                System.out.println("Произошла ошибка!");
            }
        }
    }
}
